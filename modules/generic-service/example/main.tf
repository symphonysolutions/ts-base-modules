//Local variable interpolation for service specific functionality
locals {
  backend_app_label     = "app-${var.env_level}"
  proxy_app_label       = "proxy-${var.env_level}"
  proxy_config_map_name = "proxy-conf-${var.env_level}"

  volumes = [
    {
      name = "proxy-config-${var.env_level}"

      config_map = [
        {
          name = "${local.proxy_config_map_name}"

          items = [
            {
              key  = "config"
              mode = 0755
              path = "default.conf"
            },
          ]
        },
      ]
    },
  ]

  volume_mounts = [
    {
      name       = "proxy-config-${var.env_level}"
      mount_path = "/etc/nginx/conf.d"
      read_only  = true
    },
  ]

  ports = [
    {
      name           = "app"
      container_port = 8080
      protocol       = "TCP"
    },
  ]

  service_ports = [
    {
      name        = "app"
      port        = 80
      target_port = 8080
    },
  ]
}

//Config map example for proxy service, potentially can be moved to base modules
resource "kubernetes_config_map" "proxy_conf" {
  metadata {
    name = "${local.proxy_config_map_name}"
  }

  data {
    config = <<EOF
    server {
      listen       80;
      server_name  localhost;
      location / {
        proxy_pass http://${local.backend_app_label};
      }
    }
    EOF
  }
}

module "app_server" {
  source                 = "../"
  app_label              = "${local.backend_app_label}"
  desired_replicas_count = 3

  //NOTE: Image name hardcoded in the resource definition so it wouldn't be possible to override it in higher level module
  image_name        = "tomcat"
  image_tag         = "${var.app_server_tag}"
  service_type      = "ClusterIP"
  health_check_port = 8080
  ports             = "${local.ports}"
  service_ports     = "${local.service_ports}"
}

module "proxy_server" {
  source                 = "../"
  app_label              = "${local.proxy_app_label}"
  desired_replicas_count = 3

  //NOTE: Image name hardcoded in the resource definition so it wouldn't be possible to override it in higher level module
  image_name    = "nginx"
  image_tag     = "${var.proxy_server_tag}"
  service_type  = "NodePort"
  volumes       = "${local.volumes}"
  volume_mounts = "${local.volume_mounts}"
}
