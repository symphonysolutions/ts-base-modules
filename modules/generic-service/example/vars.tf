variable "env_level" {
  default = "dev"
}

variable "app_server_tag" {
  default = "8.0-alpine"
}

variable "proxy_server_tag" {
  default = "1.12-alpine"
}
