locals {
  //Generates unique sha1 string for a configuration
  rc_conf_hash = "${sha1(
    "
    ${var.app_label}
    ${jsonencode(var.env_vars)}
    ${jsonencode(var.ports)}
    ${jsonencode(var.volumes)}
    ${jsonencode(var.volume_mounts)}
    ${var.health_check_port}
    ${var.image_name}
    ${var.image_tag}
    "
    )}"
}

resource "kubernetes_replication_controller" "rc" {
  metadata {
    //this field will generate unique name with a prefix
    //enables update with zero down time
    generate_name = "${var.app_label}-${local.rc_conf_hash}-"

    labels {
      App = "${var.app_label}"
    }
  }

  lifecycle {
    create_before_destroy = true
  }

  spec {
    replicas = "${var.desired_replicas_count}"

    selector {
      App = "${var.app_label}"
    }

    template {
      volume = "${var.volumes}"

      container {
        image = "${var.image_name}:${var.image_tag}"
        name  = "${var.app_label}"

        //Health checks
        readiness_probe {
          tcp_socket {
            port = "${var.health_check_port}"
          }

          initial_delay_seconds = "${var.app_initial_delay_seconds}"
          period_seconds        = "${var.health_check_period_seconds}"
        }

        liveness_probe {
          tcp_socket {
            port = "${var.health_check_port}"
          }

          initial_delay_seconds = "${var.app_initial_delay_seconds}"
          period_seconds        = "${var.health_check_period_seconds}"
        }

        port = "${var.ports}"

        env = "${var.env_vars}"

        volume_mount = "${var.volume_mounts}"
      }
    }
  }

  //Hack, needed to delay existing resource termination to evade downtime
  provisioner "local-exec" {
    command = "sleep ${var.app_startup_delay}"
  }
}

resource "kubernetes_horizontal_pod_autoscaler" "hpa" {
  count = "${var.create_hpa == true ? 1:0 }"

  metadata {
    name = "${kubernetes_replication_controller.rc.metadata.0.name}"
  }

  spec {
    max_replicas                      = "${var.max_replicas_count}"
    min_replicas                      = "${var.min_replicas_count}"
    target_cpu_utilization_percentage = "${var.target_cpu_utilization_percentage}"

    scale_target_ref {
      kind = "ReplicationController"
      name = "${kubernetes_replication_controller.rc.metadata.0.name}"
    }
  }

  lifecycle {
    create_before_destroy = true
  }
}

resource "kubernetes_service" "svc" {
  metadata {
    name = "${var.app_label}"
  }

  spec {
    selector {
      App = "${kubernetes_replication_controller.rc.metadata.0.labels.App}"
    }

    port = "${var.service_ports}"

    type = "${var.service_type}"
  }

  lifecycle {
    create_before_destroy = true
  }
}
