variable "image_tag" {
  default = "1.7.8"
}

variable "image_name" {
  default = "nginx"
}

variable "app_label" {
  default = "nginx"
}

variable "desired_replicas_count" {
  default = 6
}

variable "max_replicas_count" {
  default = 10
}

variable "min_replicas_count" {
  default = 3
}

variable "app_startup_delay" {
  default = "30s"
}

variable "app_initial_delay_seconds" {
  default = 10
}

variable "health_check_period_seconds" {
  default = 10
}

variable "health_check_port" {
  default = 80
}

variable "target_cpu_utilization_percentage" {
  default = 75
}

variable "service_type" {
  default = "ClusterIP"
}

variable "create_hpa" {
  default = false
}

variable "ports" {
  type = "list"

  default = [
    {
      name           = "http"
      container_port = 80
      protocol       = "TCP"
    },
  ]
}

variable "service_ports" {
  type = "list"

  default = [
    {
      name        = "http"
      port        = 80
      target_port = 80
    },
  ]
}

variable "env_vars" {
  type = "list"

  default = [
    {
      name  = "test_var"
      value = "void"
    },
    {
      name  = "test_var1"
      value = "void"
    },
  ]
}

variable "volumes" {
  type    = "list"
  default = []
}

variable "volume_mounts" {
  type    = "list"
  default = []
}
